#include "udp_uart_common.h"

#ifdef D_RUN_IN_PC
#include <errno.h>
#include  <sys/socket.h>
#include  <sys/mman.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
//
//==================================================================
#define UDP_MY_PORT  9302

//==================================================================
int udp_sim_uart_init(void){
	int sock_fd;
	sock_fd=socket(AF_INET, SOCK_DGRAM, 0);  
    	if(sock_fd < 0 ){ 
		log_err("udp socket create Error: %s\n",strerror(errno));
        	return -1;  
    	}
	
	struct sockaddr_in my_addr;
	memset(&my_addr,0,sizeof(my_addr));
	my_addr.sin_family = AF_INET;  
    	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);  
    	my_addr.sin_port = htons(UDP_MY_PORT); 

	if(bind(sock_fd,(struct sockaddr *)&my_addr, sizeof(my_addr))<0){  
        log_err("udp bind Error: %s\n",strerror(errno));
        return -1;   
    } 	

	set_noblcok(sock_fd);		
	g_skel.uart_fd=sock_fd;
	add_event(g_skel.uart_fd,EPOLLIN);	
	return 0;
}

int udp_sim_uart_out(char*buf,int len){
	for(int i=0;i<len;i++){
		printf("%02x",buf[i]);
	}	
	printf("\n");
	return 0;
}


#endif
